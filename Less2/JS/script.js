var todos = [];
var curr = null;

var list = $('#list');
var input = $('#input');
var btnAdd = $('#add-to-list');
var btnRemove = $('#remove-from-list');

function updateList() {
    list.html('');
    todos.forEach((element,index) => {
        list.append('<li id="item-'+index+'" onClick="setCurr(this,'+index+');" onMouseOver="setHover(this,true);" onMouseOut="setHover(this,false)">'+element+'</li>');
    },this);
}

function setCurr(elem,index) {
    if($(elem).hasClass("selected")) {
        $(elem).removeClass("selected");
        curr = null;
    } else {
        curr = index;
        $(elem).addClass("selected");
    }
    console.log('curr',curr);
}

function setHover(elem,isHover) {
    if(isHover) {
        $(elem).addClass("hover");
    } else {
        $(elem).removeClass("hover");
    }
}

function addToList() {
    if(input.val().length > 0) {
        todos.push(input.val());
        input.val('');
        updateList();
    }
}

function removeFromList() {
    if(curr != null) {
        console.log('remove',curr);
        todos.splice(curr,1);
        curr = null;
        updateList();
    }
}