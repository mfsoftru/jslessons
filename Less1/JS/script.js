var num = 7;
document.getElementById('input').value = num;

function start() {
    num = document.getElementById('input').value;
    var rows = [];
    for(var i = 0; i < num; i++) {
        rows[i] = Math.random();
    }
    writeLine(rows);
}

function writeLine(arr = null) {
    if (arr == null) {
        num = 0;
        document.getElementById('input').value = num;
        document.getElementById('list').innerHTML = '';
    } else {
        document.getElementById('list').innerHTML = '';
        arr.forEach( number => {
            var newLi = document.createElement('li');
            newLi.innerHTML = 'Случайный номер: '+number;
            document.getElementById('list').appendChild(newLi);
        }, this);
    }
}